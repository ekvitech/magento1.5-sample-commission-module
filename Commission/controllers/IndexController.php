<?php
/**
 *
 * Module :- Frontend Commision module for sellers 
 * Edition :- community
 * Developed By :- Arvind Rawat
 * 
 */ 
class Classic_Commission_IndexController extends Mage_Core_Controller_Front_Action
{

	
	public function preDispatch()
    {
        // a brute-force protection here would be nice

        parent::preDispatch();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        $action = $this->getRequest()->getActionName();
        $pattern = '/^(create|login|logoutSuccess|forgotpassword|forgotpasswordpost|confirm|confirmation)/i';
        if (!preg_match($pattern, $action)) {
            if (!$this->_getSession()->authenticate($this)) {
			             $this->setFlag('', 'no-dispatch', true);
            }
			
			$groupId  = $this->_getSession()->getCustomer()->getGroupId();
			$groupCode = Mage::getModel ('customer/group')->load ($groupId)->getCode(); 	
			if(!(strtolower($groupCode) == "seller"))
			{
				 return $this->_redirect('customer/account/index');
			}
				
        } else {
            $this->_getSession()->setNoReferer(true);
        }
    }


	/**
     * Retrieve customer session model object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }
	 /**
     * Default customer account page
     */
    public function indexAction()
    {
		$this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Commission Management'));

		$blockCal = $this->getLayout()->createBlock(
	    'Mage_Core_Block_Html_Calendar',
	    'html_calendar',
			array('template' => 'page/js/calendar.phtml')
		);
		$this->getLayout()->getBlock('content')->append($blockCal);
		$this->renderLayout();
	}
	//returns uploaded image 
	public function imageAction()
    {
		$params = $this->getRequest()->getParams();		
		
		$design = Mage::getModel("design/design")->load($params['id']);	
		 
		$imageUrl = Mage::getBaseUrl().'media/uploadimages/'.$design['file_name'];			
	
		echo '<img src="'.$imageUrl.'" height="527px"/>';
    }
    //Export sales commmsion data in excel format.
	public function exportAction()
	{
				$commissionorders = Mage::getResourceModel('sales/order_item_collection');
				$commissionorders->join('order', 'order_id=`order`.entity_id',array('increment_id','status','state'));
				$commissionorders->join('catalog/product', 'product_id=`catalog/product`.entity_id');
				$commissionorders->join('catalog/customer', '`catalog/product`.entity_id=`catalog/customer`.product_id');
				//$orders->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
				$commissionorders->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()));
				
				$seller_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
				$commissionorders->getSelect()->where("seller_id = '$seller_id'");
				$commissionorders->addFieldToSelect('*');
				$commissionorders->setOrder('order.created_at', 'desc');
				$commissionorders->load();
				$filename = "commission_data_" . date('Ymd') . ".xls";
				header("Content-Disposition: attachment; filename=\"$filename\"");
				header("Content-Type: application/vnd.ms-excel"); $flag = false; 
				
				$coloumHeaders = array('Model', 'Design Name','Quantity Sold','Commission Type','Commission Earned','Order Date','Commission Payment Date','Order Status','Payment Method');
			    echo implode("\t", array_values($coloumHeaders)) . "\r\n"; $flag = true; 
				
				foreach ($commissionorders as $commissionorder){
							$values = array();
							$productModel = Mage::getModel('catalog/product')->load($commissionorder->getProductId()); 
							$orderM = new Mage_Sales_Model_Order(); $orderM->load($commissionorder->getOrderId());
							 $cat = $productModel->getCategoryIds();
							$values[] =  Mage::getModel('catalog/category')->load($cat[2])->getName();
							$design_Id = $commissionorder->getDesignID();
							$designModel = Mage::getModel('upload/upload')->load($design_Id);
							$values[] = $designModel->getName();
							$values[] = $commissionorder->getQtyOrdered(); 
							$commission_type = $designModel->getCommissionType();
							$values[] = strtoupper($commission_type);
							$addedPrice = 0;
							if(strtolower($commission_type) == 'basic')
							{
									$addedPrice = $designModel->getCommissionRate();
							}
						    else
							 {
								$commissions =  Mage::getModel("upload/commission")->getCollection()
								->addFieldToFilter('design_id',array('eq' => $design_Id))
								->addFieldToFilter('case_id',array('eq' => $commissionorder->getProductId()));
								$commissionDetails  = $commissions->getData();
								$addedPrice = $commissionDetails[0]['added_price'];
							  }
							$price = 	 $commissionorder->getPrice();
							$commission_rate = (($price * 10)/100) + $addedPrice;
							$values[] = "$$commission_rate";
							$values[] = Mage::helper('core')->formatDate($commissionorder->getCreatedAtStoreDate()); 
							$values[] = Mage::helper('core')->formatDate($orderM->getPayment()->getMethodInstance()->getDate());
							$values[] = $orderM->getStatus(); 
							$values[] = $orderM->getPayment()->getMethodInstance()->getTitle(); 
							echo implode("\t", array_values($values)) . "\r\n"; 
				}

			exit;
	}

	
}

