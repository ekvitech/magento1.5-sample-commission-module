<?php
/**
 *
 * Module :- Commission admin main grid controller
 * Edition :- community
 * Developed By :- Arvind Rawat
 * 
 */ 
class Classic_Commission_Adminhtml_CommissionController extends Mage_Adminhtml_Controller_Action
{
	
	
	public function indexAction()
	{
		$this->loadLayout();	
		$this->_addContent($this->getLayout()->createBlock('commission/adminhtml_maingrid_grid'));
		$this->renderLayout();
		 
	}
	//Expland Grid content
    public function expandAction()
	{
		$this->loadLayout();	
		$seller_id = $this->getRequest()->getParam('sellerId');
		if((isset($seller_id)) &&($seller_id != ""))
		{
			$this->_addContent($this->getLayout()->createBlock('commission/adminhtml_expandgrid_grid'));
		}
		$this->renderLayout();
	}
	
	//Export commission CSV
	public function exportCsvAction()
    {
        $fileName   = 'commission.csv';
        $grid       = $this->getLayout()->createBlock('commission/adminhtml_commission_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    //Export order grid to Excel XML format     
    public function exportExcelAction()
    {
        $fileName   = 'commission.xml';
        $grid       = $this->getLayout()->createBlock('commission/adminhtml_commission_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }	
	
}