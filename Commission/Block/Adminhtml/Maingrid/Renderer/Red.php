<?php
/**
 *
 * Module :- Commission Module ExpandGrid generate manage view link 
 * Edition :- community
 * Developed By :- Arvind Rawat
 * 
 */ 

class Classic_Commission_Block_Adminhtml_Maingrid_Renderer_Red extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	 public function render(Varien_Object $row)
	{
		$seller_id =  $row->getData($this->getColumn()->getIndex());
		
		return '<a href="' . Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('commission/adminhtml_commission/expand',array('sellerId' => $seller_id)) . '">' . "View</a>";			
	}
}
?>