<?php
/**
 *
 * Module :- Commission Module ExpandGrid generate manage commision link 
 * Edition :- community
 * Developed By :- Arvind Rawat
 * 
 */ 
class Classic_Commission_Block_Adminhtml_Expandgrid_Renderer_Red extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$item_id =  $row->getData($this->getColumn()->getIndex());
		$sales_order_item = Mage::getModel('sales/order_item')->load($item_id);
		$order_id =  $sales_order_item->getOrderId();
		$product_id = $sales_order_item->getProductId();
		$seller_id = $sales_order_item->getSellerId();
		$design_id = $sales_order_item->getDesignId();
		return '<a href="' . Mage::getModel('adminhtml/url')->addSessionParam()->getNewUrl('admin/customer/commissionedit',array('order_id' => $order_id, 'product_id' => $product_id,'design_id' => $design_id,'seller_id' => $seller_id)) . '">' . "Manage Commssion </a>";				
	}
}

?>